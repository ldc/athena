#include "TrkVertexTools/VertexCollectionSortingTool.h"
#include "TrkVertexTools/VertexMergingTool.h"
#include "TrkVertexTools/DummyVertexSelectionTool.h" 
#include "TrkVertexTools/TruthVertexSelectionTool.h"  
#include "TrkVertexTools/VertexStoringTool.h"
 
using namespace Trk;
 
DECLARE_COMPONENT( VertexCollectionSortingTool )
DECLARE_COMPONENT( VertexMergingTool )
DECLARE_COMPONENT( DummyVertexSelectionTool )
DECLARE_COMPONENT( TruthVertexSelectionTool )
DECLARE_COMPONENT( VertexStoringTool )
    
 

 

